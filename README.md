# WebScraping_Sentiment_Analysis

For a company's marketing department, it is useful to know how the products are perceived by the enthuasiasts (not only the journalists) communicating on a social media. Even though they may not be the target group, who buys products, it is still good to know their attitude towards the new product.In the notebook below I will build a pipeline that extracts the text from a product review and related forum thread in www.pistonheads.com, evaluates their sentiment/polarity and visualises the result. 
Several pre-trained models will be tested to check their performance and to pick one if the results are satisfactory. This will help to avoid using labeled dataset to train the classifier from scratch.

Here is the example of the review - https://www.pistonheads.com/road-tests/porsche/2020-porsche-taycan-turbo-s--uk-review/42115 
And the forum link with the comments - https://www.pistonheads.com/gassing/topic.asp?t=1861613 

Plan is following:
 - Both links will be scraped using BeautifulSoup and cleaned before passing to the model.
 - Test several models and subjectively check how well they are able to classify.
 - Run the most acceptable model and visualise sentiment distribution. 


Future improvements:
 - To implement aspect based sentiment analysis to get a more fine-grained insights on different aspects, for example price, design, handling, technology.
 - To see if there is a way to get analysis by using only article link. Right now there is no direct link in the HTML to the forum posts, even though it is visible below the article.